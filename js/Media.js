(function(global, $) {

  var Media = function(id, isEditable) {
    return new Media.init(id, isEditable);
  }

  Media.prototype = {};

  Media.init = function(id, isEditable) {
    var self = this;
    self.id = id;
    self.isEditable = isEditable;
  }

  Media.init.prototype = Media.prototype;

  global.Media = global.M$ = Media;

}(window, jQuery));
